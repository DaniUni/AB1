package at.danidipp.aau;

import java.util.Scanner;

public class Aufgabe8 {

    public static void doStuff(Scanner sc){
        System.out.print("Radius: ");
        double r = sc.nextInt();

        double circumference = 2*r*Math.PI;
        double area = Math.PI*r*r;

        System.out.println("Umfang: " + circumference);
        System.out.println("Fläche: " + area);
    }
}
