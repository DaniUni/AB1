package at.danidipp.aau;

public class Aufgabe10 {

    public static void do1(){
        int a = 10;
        float b = 1.1f; //Deklaration von Float mit f-suffix
        System.out.println(a+b);
    }

    public static void do2(){
        int a = 10;
        double b = 1.1;
        double c = a+b; //Addierung von int und double inkompatibel
        System.out.println(c);
    }

    public static void do3(){
        int a = 10;
        double b = 1.1;
        int c = a+(int)b; //Kein Fehler, aber beim Typecast gehen Nachkommastellen verloren.
        System.out.println(c);
    }

    public static void do4(){
        float a = 0;
        double b = 1.1;
        double c = a+b;
        System.out.println(c);
    }

    public static void do5(){
        float a = 0.9f; //Deklaration von Float mit f-suffix
        double b = 1.1;
        int c = (int)a+(int)b; //Nachkommastellenverlust bei Typecast
        System.out.println(c);
    }

    public static void do6(){
        float a = 0;
        double b = 1.1;
        int c = 0;
        try{
            c = (int)b/ (int)a;
        }catch(ArithmeticException e){} //Dividing by 0 will cause an Exception
        System.out.println(c);
    }

    public static void do7(){
        int a = 45;
        int b = a;
        long c = Integer.MAX_VALUE + 1; //Int ist nicht groß genug.
        a = a*a;
        b = ((b*b)/b)%b; //Dritte ) zu viel
        System.out.println("a="+a+" b="+b+" c="+c);
    }
}
