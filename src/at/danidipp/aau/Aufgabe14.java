package at.danidipp.aau;

import java.util.Scanner;

public class Aufgabe14 {

    public enum Weekdays{
        Montag, Dienstag, Mittwoch, Donnerstag, Freitag, Samstag, Sonntag
    }
    public static void doStuff(Scanner sc){

        System.out.println(Weekdays.values()[sc.nextInt()-1].toString()); //Handling von Zahlen außerhalb von 1-7 ist nicht in der Aufgabe
    }
}
