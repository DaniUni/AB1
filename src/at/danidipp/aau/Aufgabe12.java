package at.danidipp.aau;

import java.util.Scanner;

public class Aufgabe12 {

    public static void doStuff(Scanner sc){
        int year = sc.nextInt();

        if(year == 0 || year % 400 == 0) System.out.println("Schaltjahr");
        else if(year % 100 == 0) System.out.println("Kein Schaltjahr");
        else if(year % 4 == 0) System.out.println("Schaltjahr");
        else System.out.println("Kein Schaltjahr");
    }
}
