package at.danidipp.aau;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Aufgabe1.doStuff();
        Aufgabe2.doStuff();
        Aufgabe3.doStuff();
        Aufgabe4.doStuff();
        Aufgabe5.doStuff();
        Aufgabe6.doStuff();
        Aufgabe7.doStuff(sc);
        Aufgabe8.doStuff(sc);
        Aufgabe9.doStuff(sc);

        Aufgabe10.do1();
        Aufgabe10.do2();
        Aufgabe10.do3();
        Aufgabe10.do4();
        Aufgabe10.do5();
        Aufgabe10.do6();
        Aufgabe10.do7();

        Aufgabe11.doStuff(sc);
        Aufgabe12.doStuff(sc);
        Aufgabe13.doStuff(sc);
        Aufgabe14.doStuff(sc);
    }
}
