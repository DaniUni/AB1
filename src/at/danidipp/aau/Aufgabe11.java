package at.danidipp.aau;

import java.util.Scanner;

public class Aufgabe11 {

    public static void doStuff(Scanner sc){
        String mark = "";

        switch ((int)(sc.nextInt()-.5)/50){
            //Negative Punkte werden von der Aufgabenstellung nicht behandelt.
            //Das Programm behandelt -1 .. -49 als "Nicht genügend" und alles darunter als default.
            case 0:
            case 1:
            case 2:
            case 3:
                mark = "Nicht genügend";
                break;
            case 4:
                mark = "Genügend";
                break;
            case 5:
                mark = "Befriedigend";
                break;
            case 6:
                mark = "Gut";
                break;
            case 7:
                mark = "Sehr gut";
                break;
            default:
                mark = "Da ist wohl was schief gegangen";
                break;
        }

        System.out.println(mark);
    }
}
