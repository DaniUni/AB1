package at.danidipp.aau;

import java.util.Scanner;

public class Aufgabe7 {

    public static void doStuff(Scanner sc){

        System.out.print("x: ");
        int x = sc.nextInt();

        System.out.print("y: ");
        int y = sc.nextInt();

        int z = 2*x+y*y;
        System.out.println("Das Ergebnis von 2x+y^2 ist " + z);
    }
}
