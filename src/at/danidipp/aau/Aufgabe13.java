package at.danidipp.aau;

import java.util.Scanner;

public class Aufgabe13 {

    public static void doStuff(Scanner sc){
        int i = sc.nextInt();

        if(i % 5 != 0){
            System.err.println("Betrag würde Münzen benötigen");
        } else {
            int i100 = i / 100;
            i = i - (i / 100)*100;
            int i50 = i / 50;
            i = i - (i / 50)*50;
            int i20 = i / 20;
            i = i - (i / 20)*20;
            int i10 = i / 50;
            i = i - (i / 10)*10;
            int i5 = i / 5;
            i = i - (i / 5)*5;

            System.out.println("100: " + i100);
            System.out.println("50: " + i50);
            System.out.println("20: " + i20);
            System.out.println("10: " + i10);
            System.out.println("5: " + i5);
            //System.out.println("Rest: " + i);
        }
    }
}
