package at.danidipp.aau;

public class Aufgabe4 {

    public static void doStuff(){
        int i1 = -32;
        float f1 =6.24f;
        double d1 = 1.968;
        float f2 = 1.00e+3f;
        double d2 = 1.00e-3;
        int i2 = 1624;
        String s1 = "1000";
        String s2 = "A";
        char c1 = 'A';
        boolean b1 = false;
        char c2 = '\n';
        String s3 = "\n";
        int i3 = 0xA5;
        char c3 = '\u0065';
        int i4 = -25874;

        String temp = i1 + "," + f1  + "," + d1  + "," + f2  + "," + d2  + "," +
                i2  + "," + s1  + "," + s2  + "," + c1  + "," +b1  + "," +c2  + "," +
                s3  + "," + i3  + "," + c3  + "," + i4;
                System.out.println(temp);
    }
}
