package at.danidipp.aau;

import java.util.Scanner;

public class Aufgabe9 {

    public static void doStuff(Scanner sc){
        System.out.print("zurückgelegte Kilometer: ");
        double distance = sc.nextInt();

        System.out.print("benötigte Treibstoffmenge: ");
        double fuel = sc.nextInt();

        System.out.println((100/distance)*fuel);
    }
}
